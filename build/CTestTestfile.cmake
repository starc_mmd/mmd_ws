# CMake generated Testfile for 
# Source directory: /home/v/saw_ws/src
# Build directory: /home/v/saw_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(intera_common/intera_common)
subdirs(intera_sdk/intera_sdk)
subdirs(intera_common/intera_tools_description)
subdirs(sawyer_robot/sawyer_description)
subdirs(sawyer_moveit/sawyer_moveit)
subdirs(sawyer_robot/sawyer_robot)
subdirs(sawyer_simulator/sawyer_simulator)
subdirs(ar_track_alvar/ar_track_alvar_msgs)
subdirs(sawyer_simulator/sawyer_hardware_interface)
subdirs(intera_common/intera_core_msgs)
subdirs(intera_common/intera_motion_msgs)
subdirs(intera_sdk/intera_interface)
subdirs(sawyer_simulator/sawyer_sim_examples)
subdirs(ar_track_alvar/ar_track_alvar)
subdirs(robot_setup_tf)
subdirs(sawyer_moveit/sawyer_moveit_config)
subdirs(sawyer_simulator/sawyer_sim_controllers)
subdirs(sawyer_simulator/sawyer_gazebo)
subdirs(using_markers)
subdirs(intera_sdk/intera_examples)
