#!/bin/sh

if [ -n "$DESTDIR" ] ; then
    case $DESTDIR in
        /*) # ok
            ;;
        *)
            /bin/echo "DESTDIR argument must be absolute... "
            /bin/echo "otherwise python's distutils will bork things."
            exit 1
    esac
    DESTDIR_ARG="--root=$DESTDIR"
fi

echo_and_run() { echo "+ $@" ; "$@" ; }

echo_and_run cd "/home/v/saw_ws/src/intera_sdk/intera_examples"

# ensure that Python install destination exists
echo_and_run mkdir -p "$DESTDIR/home/v/saw_ws/install/lib/python2.7/dist-packages"

# Note that PYTHONPATH is pulled from the environment to support installing
# into one location when some dependencies were installed in another
# location, #123.
echo_and_run /usr/bin/env \
    PYTHONPATH="/home/v/saw_ws/install/lib/python2.7/dist-packages:/home/v/saw_ws/build/lib/python2.7/dist-packages:$PYTHONPATH" \
    CATKIN_BINARY_DIR="/home/v/saw_ws/build" \
    "/usr/bin/python" \
    "/home/v/saw_ws/src/intera_sdk/intera_examples/setup.py" \
    build --build-base "/home/v/saw_ws/build/intera_sdk/intera_examples" \
    install \
    $DESTDIR_ARG \
    --install-layout=deb --prefix="/home/v/saw_ws/install" --install-scripts="/home/v/saw_ws/install/bin"
