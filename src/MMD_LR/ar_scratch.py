#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Title: AR scratch

Author: Luke Ramos
Year: 2018

Description: script designed to perform tracking of an AR Tag using ar_track_alvar.
Aim is to design a P-Controller that is able to match end-effector pose with ar-tag pose.
A variant of Intera Examples joint_torque_springs.

"""
import argparse
import importlib

import rospy
from dynamic_reconfigure.server import Server
from geometry_msgs.msg import (WrenchStamped, PoseStamped,)
from utilities import *

from geometry_msgs.msg import (
    PoseStamped,
    Pose,
    Point,
    Quaternion,
    TransformStamped,
)

from std_msgs.msg import (Header, String, Float64, Empty)
from sensor_msgs.msg import JointState
from operator import itemgetter
import numpy as np

import intera_interface
from intera_interface import CHECK_VERSION

from intera_core_msgs.srv import (
    SolvePositionIK,
    SolvePositionIKRequest,
)

from roslib import message
from numpy import linalg
import math
import tf
import threading
from ar_track_alvar_msgs.msg import *

#MARKER_ID_DETECTION = 20

class PControl(object):
    def __init__(self, reconfig_server, limb = "right"):
        self._dyn = reconfig_server

        self.listener = tf.TransformListener()
        self.transformer = tf.TransformerROS()

        # control parameters
        self._rate = 1000.0  # Hz
        self._missed_cmds = 20.0  # Missed cycles before triggering timeout

        # create our limb instance
        self._limb = intera_interface.Limb(limb)

        # initialize parameters
        self._start_angles = dict()

        # verify robot is enabled
        print("Getting robot state... ")
        self._rs = intera_interface.RobotEnable(CHECK_VERSION)
        self._init_state = self._rs.state().enabled
        print("Enabling robot... ")
        self._rs.enable()
        print("Running. Ctrl-c to quit")

        # Subscribers
        self.sub_ar_pose_marker = rospy.Subscriber('/ar_pose_marker', AlvarMarkers, self.cbGetMarker, queue_size = 1)
#        print "Subscribed to AR Track Alvar Topic"

        # make an object of the correct type then initialise the variables
        point = Point(.0,.0,.0)
        self.marker_position = point

        quatern = Quaternion(.0,.0,.0,.0)
        self.marker_orientation = quatern

        self.ps = PoseStamped()
        self.ps.pose.position = Point(.0,.0,.0)
        self.ps.pose.orientation = Quaternion(.0,.0,.0,.0)
        self.marker_ps = self.ps

        loop_rate = rospy.Rate(10)

        self.start_q = self._limb.joint_angles()

        self.trans_pose = self.ps

    def cbGetMarker(self, markers_msg, limb = "right"):
        for marker in markers_msg.markers:
            self.marker_ps = marker.pose
            print "this is raw", self.marker_ps

            look_trans = self.listener.lookupTransform('/base', '/right_hand', self.listener.getLatestCommonTime('/base', '/right_hand'))
            print "this is the transformation", look_trans
            trans_pose = self.transformer.transformPose(self.marker_ps.header.frame_id, self.marker_ps)
#            print "this is trans", trans_pose

            ns = "ExternalTools/" + limb + "/PositionKinematicsNode/IKService"

            iksvc = rospy.ServiceProxy(ns, SolvePositionIK)
            ikreq = SolvePositionIKRequest()

            hdr = Header(stamp=rospy.Time.now(), frame_id='base')
            poses = {'right': PoseStamped(
                                            header = hdr,
                                            pose = Pose(
                                            position = Point(
                                                            x = trans_pose.pose.position.x,
                                                            y = trans_pose.pose.position.y,
                                                            z = trans_pose.pose.position.z,
                                                    ),
                                            orientation = Quaternion(
                                                            x = trans_pose.pose.orientation.x,
                                                            y = trans_pose.pose.orientation.y,
                                                            z = trans_pose.pose.orientation.z,
                                                            w = trans_pose.pose.orientation.w,
                                                    ),
                                            ),
                                    ),
                            }


#            print "this is transformed pose", poses

            ikreq.pose_stamp.append(poses[limb])
            ikreq.tip_names.append('right_hand')


            rospy.wait_for_service(ns, 5.0)
            resp = iksvc(ikreq)

            limb_joints = dict(zip(resp.joints[0].name, resp.joints[0].position))

            global sub_right_j0
            global sub_right_j1
            global sub_right_j2
            global sub_right_j3
            global sub_right_j4
            global sub_right_j5
            global sub_right_j6

            sub_right_j0 = resp.joints[0].position[0]
            sub_right_j1 = resp.joints[0].position[1]
            sub_right_j2 = resp.joints[0].position[2]
            sub_right_j3 = resp.joints[0].position[3]
            sub_right_j4 = resp.joints[0].position[4]
            sub_right_j5 = resp.joints[0].position[5]
            sub_right_j6 = resp.joints[0].position[6]

            self.joint_pos = np.array([sub_right_j0, sub_right_j1, sub_right_j2, sub_right_j3, sub_right_j4, sub_right_j5, sub_right_j6],dtype=np.float)
            self.move_to_position(self.joint_pos, 20)


    def clean_shutdown(self):
        """
        Switches out of joint torque mode to exit cleanly
        """
        print("\nExiting Program...")
        self._limb.exit_control_mode()

    def move_to_neutral(self):
        """
        Moves the limb to neutral location.
        """
        self._limb.move_to_neutral(speed=0.1)

    def move_to_position(self, joint_pos, timeout):
        cmd = make_cmd(self._limb.joint_names(), joint_pos)
        self._limb.set_joint_position_speed(0.15)
        self._limb.move_to_joint_positions(cmd, timeout=timeout)
        self._limb.set_joint_position_speed(0.15)  # speed back to default

def main():
        # Querying the parameter server to determine Robot model and limb name(s)
        rp = intera_interface.RobotParams()
        valid_limbs = rp.get_limb_names()
        if not valid_limbs:
                rp.log_message(("Cannot detect any limb parameters on this robot. "
                                                "Exiting."), "ERROR")
        robot_name = intera_interface.RobotParams().get_robot_name().lower().capitalize()
        # Parsing Input Arguments
        arg_fmt = argparse.ArgumentDefaultsHelpFormatter
        parser = argparse.ArgumentParser(formatter_class=arg_fmt)
        parser.add_argument(
                "-l", "--limb", dest="limb", default=valid_limbs[0],
                choices=valid_limbs,
                help='limb on which to attach joint springs'
                )
        args = parser.parse_args(rospy.myargv()[1:])
        # Grabbing Robot-specific parameters for Dynamic Reconfigure
        config_name = ''.join([robot_name,"JointSpringsExampleConfig"])
        config_module = "intera_examples.cfg"
        cfg = importlib.import_module('.'.join([config_module,config_name]))
        # Starting node connection to ROS
        print("Initializing node... ")
        rospy.init_node("sdk_joint_torque_springs_{0}".format(args.limb))
        dynamic_cfg_srv = Server(cfg, lambda config, level: config)
        PC = PControl(dynamic_cfg_srv, limb=args.limb)
        # register shutdown callback
        rospy.on_shutdown(PC.clean_shutdown)

        rospy.spin()

if __name__ == "__main__":
    main()
