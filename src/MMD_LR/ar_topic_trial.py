#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import importlib
import rospy
from roslib import message

from geometry_msgs.msg import (WrenchStamped, PoseStamped,)
from std_msgs.msg import (Header, String, Float64, Empty)
from geometry_msgs.msg import (
    PoseStamped,
    Pose,
    Point,
    Quaternion,
    TransformStamped,
)
from sensor_msgs.msg import JointState
#from ar_track_alvar import AlvarMarkers
from ar_track_alvar_msgs.msg import *

class AR_Topic(object):
    def __init__(self):
        self._sub_ar = rospy.Subscriber("/ar_pose_marker", AlvarMarkers, self.ar_Callback)
#        print "Subscribed to AR Track Alvar Topic"

    def ar_Callback(self, msg):
        rospy.loginfo("callback triggered")
        ar_msg = msg
#        pose
        print "printing", ar_msg

    def sample_print(self, ar_msg):
        print "this is printing", ar_msg
#        print "try this"

def main():
    AR = AR_Topic()
    AR.sample_print

    rospy.init_node("ar_topic_node")

if __name__ == '__main__':
    main()
