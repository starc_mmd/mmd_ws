#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Title: AR Tracking

Author: Luke Ramos
Year: 2018

Description: script designed to perform tracking of an AR Tag using ar_track_alvar

"""
import argparse
import importlib
import rospy
from roslib import message
import intera_interface
from intera_interface import CHECK_VERSION
from intera_core_msgs.srv import (
    SolvePositionIK,
    SolvePositionIKRequest,
)

from geometry_msgs.msg import (WrenchStamped, PoseStamped,)
from std_msgs.msg import (Header, String, Float64, Empty)
from geometry_msgs.msg import (
    PoseStamped,
    Pose,
    Point,
    Quaternion,
    TransformStamped,
)
from sensor_msgs.msg import JointState
from config import CONFIGURATION
import numpy as np
from numpy import linalg
import math

import threading
from util.kinematics import manipulator_jacobian, jacobian_n, jacobn_rec, Quat2rpy
from util.kinematics import get_angle_list, forward_kinematics, t2Rot

import control
from dynamic_reconfigure.server import Server

#class ARClass(object):
#    def __init__(self):
#        self._event = threading.Event()
#        self._msg = None

#    def __call__(self, msg):
#        self._msg = msg
#        self._event.set()

#    def get_msg(self, timeout=None):
#        self._event.wait(timeout)
#        return self._msg

class PControl(object):
    """
    Virtual Joint Springs class for torque example.

    @param limb: limb on which to run joint springs example
    @param reconfig_server: dynamic reconfigure server

    JointSprings class contains methods for the joint torque example allowing
    moving the limb to a neutral location, entering torque mode, and attaching
    virtual springs.
    """

    def __init__(self, reconfig_server, limb = "right"):
        self._dyn = reconfig_server

        # control parameters
        self._rate = 1000.0  # Hz
        self._missed_cmds = 20.0  # Missed cycles before triggering timeout

        # ROS control parameters:older
        #self._rate = 150.0  # Hz
        #self._missed_cmds = 20.0  # Missed cycles before triggering timeout

        #SET CONTROL REFERENCES
#        self.AR_track = np.zeros((7, 1))
        self.reference = np.zeros((7, 1))
        self.reference[0] = 0  # x
        self.reference[1] = 0  # y
        self.reference[2] = 0  # z
        self.reference[3] = 0  # qx
        self.reference[4] = 0  # qy
        self.reference[5] = 0  # qz
        self.reference[6] = 0  # qw

        # initial control action
        self.joint_vel = np.zeros((7, 1))
        self.K = 0.05
        self._done = False

        # create our limb instance
        self._limb = intera_interface.Limb(limb)

        # initialize parameters
        self._start_angles = dict()

        # verify robot is enabled
        print("Getting robot state... ")
        self._rs = intera_interface.RobotEnable(CHECK_VERSION)
        self._init_state = self._rs.state().enabled
        print("Enabling robot... ")
        self._rs.enable()
        print("Running. Ctrl-c to quit")

        # Subscribers
        self._sub_ar = rospy.Subscriber("/ar_pose_marker", AlvarMarker, ar_Callback)
        print "Subscribed to AR Track Alvar Topic"
#        self._ig = ARClass()

        self.start_q = self._limb.joint_angles()

    def ar_Callback(self, msg):
        rospy.loginfo("Tracker message received")
        ar_position = msg.pose.position
        ar_orientation = msg.pose.orientation
        rospy.loginfo("Point Position: [ %f, %f, %f ]"%(position.x, position.y, position.z))
        rospy.loginfo("Quat Orientation: [ %f, %f, %f, %f]"%(quat.x, quat.y, quat.z, quat.w))
        P = [msg.pose.position.x, msg.pose.position.y, msg.pose.position.z]
        Q = [msg.pose.orientation.x, msg.pose.orientation.y, msg.pose.orientation.z, msg.pose.orientation.w]
        return P, Q

    def clean_shutdown(self):
        """
        Switches out of joint torque mode to exit cleanly
        """
        print("\nExiting example...")
        self._limb.exit_control_mode()

    def move_to_neutral(self):
        """
        Moves the limb to neutral location.
        """
        self._limb.move_to_neutral(speed=0.1)

    def move_to_position(self, joint_pos, timeout):
        cmd = self.make_cmd(self._limb.joint_names(), joint_pos)
        self._limb.set_joint_position_speed(0.15)
        self._limb.move_to_joint_positions(cmd, timeout=timeout)
        self._limb.set_joint_position_speed(0.15)  # speed back to default

    def make_cmd(self, joints, _cmd):
        return dict([(joint, _cmd.item(i))
                     for i, joint in enumerate(joints)])

#    def get_sensor_data(self):
#        """
#        Get sensor info
#        :return:
#        """
#        msg = self._ig.get_msg()  # Wrench info
#        P = [msg.pose.position.x, msg.pose.position.y, msg.pose.position.z]
#        Q = [msg.pose.orientation.x, msg.pose.orientation.y, msg.pose.orientation.z, msg.pose.orientation.w]
#        return P, Q

    def p_Controller_pose(ang, ar_position)
        px = position[0]
        py = position[1]
        pz = position[2]

        pxref = self.reference[0]
        pyref = self.reference[1]
        pzref = self.reference[2]

        errorpx = (pxref - px)
        errorpy = (pyref - py)
        errorpz = (pzref - pz)

        vx = 0.0005 * errorpx
        vy = 0.0005 * errorpy
        vz = 0.0005 * errorpz

        v7 = np.mat([0, 0, 0])
        v7 = np.transpose(v7)

        Ti = forward_kinematics(ang)
        Ri = t2Rot(Ti)

        v0 = Ri * v7

        print 'vx:', vx
        print 'errorpx', errorpx
        print 'vy:', vy
        print 'errorpy', errorpy
        print 'vz:', vz
        print 'errorpz', errorpz

#    def p_Controller_quat(self, msg, ar_orientation)
#        qx = orientation[0]
#        qx = orientation[1]
#        qx = orientation[2]
#        qx = orientation[3]

#        qxref = self.reference[3]
#        qyref = self.reference[4]
#        qzref = self.reference[5]
#        qwref = self.reference[6]

#        errorqx = (qxref - qx)
#        errorqy = (qyref - qy)
#        errorqz = (qzref - qz)
#        errorqw = (qwref - qw)

    def compute_control_action_hierarchy(self, v0, w0, q_init, q_current):
        N0 = np.identity(7)
        N0 = np.mat(N0)
        x0 = np.zeros((7, 1))
        x0 = np.mat(x0)
        # init solution
        x = x0
        N = N0
        [x1, N] = self.kinematic_restrictionVW(q_current, v0, w0, x, N)

        x = x0
        N = N0
        [x2, N] = self.kinematic_restriction_initialq(q_init, q_current, x, N)
        # superposition
        final_x = x1 #+ x2
        return final_x

    def kinematic_restrictionVW(self, q_current, v0, w0, x, N):
        J = manipulator_jacobian(q_current)
        # row stack
        V = np.vstack((v0, w0))
        A = J
        b = V
        x = self.compute_hierarchy_sol(x, A, b, N)
        N = self.compute_hierarchy_N(A, N)
        return x, N

    def kinematic_restriction_initialq(self, q_init, q_current, x, N):
        I = np.identity(7)
        I = np.mat(I)
        A = I
        # A[1,1]=0
        q_current = np.mat(q_current)
        q_current = q_current.reshape(7, 1)
        q_init = np.mat(q_init)
        q_init = q_init.reshape(7, 1)
        b = 1.5 * (q_init - q_current)
        print 'b:', b
        x = self.compute_hierarchy_sol(x, A, b, N)
        N = self.compute_hierarchy_N(A, N)
        return x, N

    def compute_hierarchy_sol(self, xim1, Ai, bi, Nim1):
        """
        update the solution according to the new restriction
        :param xim1:
        :param Ai:
        :param bi:
        :param Nim1:
        :return:
        """
        x = xim1 + self.penrose(Ai * Nim1) * (bi - Ai * xim1)
        return x

    def compute_hierarchy_N(self, Ai, Nim1):
        I = np.identity(7)
        # using the penrose pseudo inverse
        N = Nim1 * (I - self.penrose(Ai * Nim1) * (Ai * Nim1))
        return N

    def penrose(self, C):
        """
        Compute Moore-penrose pseduo inverse
        :param C:
        :return:
        """
        return np.linalg.pinv(C)

    def control_loop(self):
        """
        The main control loop calls the member _SMC_acc_Cartesian to compute the control actions.
        The control actions are also applied by _SMC_acc_Cartesian.
        """
        # record initial joint angles
        self._start_angles = get_angle_list(self._limb, self._limb._joint_names)  # self._limb.joint_angles()
        self.q0 = get_angle_list(self._limb, self._limb._joint_names)
        print 'INIT CONTROL ACTION', self._start_angles

        # set control rate
        control_rate = rospy.Rate(self._rate)

        # for safety purposes, set the control rate command timeout.
        # if the specified number of command cycles are missed, the robot
        # will timeout and return to Position Control Mode
        self._limb.set_command_timeout((1.0 / self._rate) * self._missed_cmds)

        print 'Starting control!'
        # loop at specified rate commanding new joint torques
        while not rospy.is_shutdown() and not self._done:
            start = rospy.get_time()
            if not self._rs.state().enabled:
                rospy.logerr("Joint torque example failed to meet "
                             "specified control rate timeout.")
                break

            position, orientation = self.get_sensor_data()
            q = get_angle_list(self._limb, self._limb._joint_names)
            q0 = self._start_angles  # get_angle_list()

            # based on the forces expressed in the local system
            # compute action!
            v0 = self.p_Controller_pose(q, pose)
#            v0 = np.mat([0, 0, 0])
#            v0 = np.transpose(v0)

#            w0 = self.compute_high_level_actionW(q, torque)
            w0 = np.mat([0, 0, 0])
            w0 = np.transpose(w0)

            # based on forces and torques an Ti... and the rest
            # compute v, w
            # map to joint speeds
            # and apply speeds
            #q0 is initial
            #q is current angles
            qd = self.compute_control_action_hierarchy(v0, w0, q0, q)
            print "qd is: ", qd
            # just apply speeds
            self.joint_vel = qd

            # send the command to the arm
            cmd = self.make_cmd(self._limb.joint_names(), self.joint_vel)
            self._limb.set_joint_velocities(cmd)

            # print 'publishing'
            # js_out = JointState()
            # js_out.header.stamp = rospy.Time.now()
            # js_out.header.frame_id = 'frame_id'
            # js_out.velocity = self.joint_vel
            #
            # self.publisher.publish(js_out)

            # self._sigma_max_ant = self._sigma_max
            control_rate.sleep()
