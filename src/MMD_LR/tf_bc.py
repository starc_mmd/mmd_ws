#!/usr/bin/env python

# Author: Vincent Jadraque
# Student ID: 11970720
# Date: 29/10/2018
# File name: tf_bc.py

'''
This broadcasts the transforms between the robot hand and the camera
'''

import roslib
import rospy
import tf
import math


def rpy2q(roll, pitch, yaw):
    return tf.transformations.quaternion_from_euler(roll, pitch, yaw)

if __name__ == '__main__':
    rospy.init_node('apc_tf_broadcaster')

    tl = tf.TransformListener()
    tb = tf.TransformBroadcaster()
    rate = rospy.Rate(100)

    hand2camera_length = 0.1 # Meters
    
    while not rospy.is_shutdown():
        try:
            time = tl.getLatestCommonTime('/base', '/world')

            # realsense camera wrt hand
            # TODO check actual distances and orientation for realsense camera
            tb.sendTransform((0, 0, hand2camera_length), rpy2q(0, 0, -math.pi/4), time, "camera_rgb_optical_frame", "right_hand")
            # laptop camera wrt hand
            tb.sendTransform((0, 0, hand2camera_length), rpy2q(0, 0, -math.pi/4), time, "usb_cam","right_hand")
            
        except tf.Exception:
            rospy.loginfo("Exception Error")
            pass
        rate.sleep()