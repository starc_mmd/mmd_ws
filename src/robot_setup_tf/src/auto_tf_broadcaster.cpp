// Author: Vincent Jadraque
// Student ID: 11970720
// Date: 29/10/2018

// File name: sim_tf_broadcaster.cpp
/* 
   Description: 

for simulation - broadcasts hardcoded transform between 
the base frame and the camera frame
also adds target frame at ar marker frame with roll of 180 degrees

*/

#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <ar_track_alvar_msgs/AlvarMarkers.h>
// #include <string>

int id = 0;

void cbGetMarkerFrame(const ar_track_alvar_msgs::AlvarMarkers::ConstPtr& msg)
{
  // retrieve the frame id from the first marker in the message
  // TODO: loop through markers and search for correct marker.id
  
  try
  {
    id = msg->markers.at(0).id;
    ROS_INFO("ID is %d", id);
  }
  catch (std::out_of_range oor) {
    ROS_ERROR("%s", oor.what());
  }
}

int main(int argc, char** argv){
  ros::init(argc, argv, "robot_tf_publisher");
  ros::NodeHandle n;

  ros::Rate r(100);

  tf::TransformBroadcaster broadcaster;
  tf::TransformListener listener;
  ros::Subscriber sub = n.subscribe("ar_pose_marker", 1, cbGetMarkerFrame);

  while(n.ok()){

    // broadcaster.sendTransform(
    //   tf::StampedTransform(
    //     // quat - xyzw; translation - xyz
    //     tf::Transform(tf::Quaternion(-0.5, 0.5, -0.5, 0.5), tf::Vector3(0.0, 0.4, 0.3)),
    //     ros::Time::now(),"world", "sim_point"));

    // try
    // {
    //   // get transform of marker from usb_cam
    //   tf::StampedTransform transform;
    //   try{
    //     listener.lookupTransform(frame_id, "/usb_cam",  
    //                             ros::Time(0), transform);
    //   }
    //   catch (tf::TransformException ex){
    //     ROS_ERROR("%s",ex.what());
    //     ros::Duration(1.0).sleep();
    //   }

    //   // TODO: multiply quaternion such that it rotates 180 degrees about x axis
    //   // tf::Quaternion q_orig, q_rot, q_new;
    //   double r=3.14159, p=0, y=0;  // Rotate the previous pose by 180* about X
    //   q_rot = tf::createQuaternionFromRPY(r, p, y);
      
    //   // TODO: access position and quaternion from looked up transform
    //   // Calculate the new orientation
    //   q_new = q_rot*q_orig;
    //   q_new.normalize();
      
    //   // broadcast simulated ar marker point
    //   broadcaster.sendTransform(
    //   tf::StampedTransform(
    //     // quat - xyzw; translation - xyz
    //     // broadcast target frame applying 
    //     // 180 degree rotation about x axis
    //     tf::Transform(tf::Quaternion(1.0, 0.0, 0.0, 0.0), tf::Vector3(0.0, 0.0, 0.0)),
    //     ros::Time::now(),"sim_point", "target"));
    // }


    broadcaster.sendTransform(
      tf::StampedTransform(
        // quat - xyzw; translation - xyz
        tf::Transform(tf::Quaternion(-0.5, 0.5, -0.5, 0.5), tf::Vector3(0.0, 0.4, 0.3)),
        ros::Time::now(),"base", "usb_cam"));

    try
    {
      ros::spinOnce();
      std::string frame_id = "ar_marker_" + boost::lexical_cast<std::string>(id);

      broadcaster.sendTransform(
      tf::StampedTransform(
        // quat - xyzw; translation - xyz
        // 180 degree rotation to make target pose face the ar tag
        tf::Transform(tf::Quaternion(1.0, 0.0, 0.0, 0.0), tf::Vector3(0.0, 0.0, 0.0)),
        // TODO: make code more robust by automating the marker frame id - make callback subscriber
        ros::Time::now(), frame_id, "target"));
    }
    catch (tf::TransformException ex){
      ROS_ERROR("%s",ex.what());
      ros::Duration(1.0).sleep();
    }
    r.sleep();
  }
}
