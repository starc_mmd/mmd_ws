// Author: Vincent Jadraque
// Student ID: 11970720
// Date: 29/10/2018

// File name: sim_tf_broadcaster.cpp
/* 
   Description: 

for simulation - broadcasts hardcoded transform between 
the base frame and the camera frame
also adds target frame at ar marker frame with roll of 180 degrees

*/

#include <ros/ros.h>
#include <tf/transform_broadcaster.h>

int main(int argc, char** argv){
  ros::init(argc, argv, "robot_tf_publisher");
  ros::NodeHandle n;

  ros::Rate r(100);

  tf::TransformBroadcaster broadcaster;

  while(n.ok()){
    broadcaster.sendTransform(
      tf::StampedTransform(
        // quat - xyzw; translation - xyz
        tf::Transform(tf::Quaternion(-0.5, 0.5, -0.5, 0.5), tf::Vector3(0.0, 0.4, 0.3)),
        ros::Time::now(),"base", "usb_cam"));

    try
    {
      broadcaster.sendTransform(
      tf::StampedTransform(
        // quat - xyzw; translation - xyz
        // 180 degree rotation to make target pose face the ar tag
        tf::Transform(tf::Quaternion(1.0, 0.0, 0.0, 0.0), tf::Vector3(0.0, 0.0, 0.0)),
        // TODO: make code more robust by automating the marker frame id - make callback subscriber
        ros::Time::now(),"ar_marker_30", "target"));
    }
    catch (tf::TransformException ex){
      ROS_ERROR("%s",ex.what());
      ros::Duration(1.0).sleep();
    }
    r.sleep();
  }
}
