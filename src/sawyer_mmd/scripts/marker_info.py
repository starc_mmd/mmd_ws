#!/usr/bin/env python

# Author: Vincent Jadraque
# Student ID: 11970720
# Date: 25/10/2018
# File name: marker_info.py

import rospy
from ar_track_alvar_msgs.msg import AlvarMarkers
#from tf.transformations import euler_from_quaternion, quaternion_from_euler
import math
import time
    
#import numpy as np
#import tf
#from nav_msgs.msg import Odometry
#from geometry_msgs.msg import PoseStamped, Pose, Point
from geometry_msgs.msg import (
    PoseStamped,
    # Pose,
    Point,
    Quaternion,
)
    
MARKER_ID_DETECTION = 20

class MarkerInfo():
    def __init__(self):
        # initialise variables
        # subscribe to ar_pose_marker
        self.sub_ar_pose_marker = rospy.Subscriber('/ar_pose_marker', AlvarMarkers, self.cbGetMarkerOdom, queue_size = 1)
        # init publishers
        self.pub_marker_position = rospy.Publisher('/py_marker_position', Point, queue_size = 1)
        self.pub_marker_orientation = rospy.Publisher('/py_marker_orientation', Quaternion, queue_size = 1)
        self.pub_marker_pose_stamped = rospy.Publisher('/py_marker_ps', PoseStamped, queue_size = 1)
        
        # make an object of the correct type then initialise the variables
        point = Point(.0,.0,.0)
        self.marker_position = point
        
        quatern = Quaternion(.0,.0,.0,.0)
        self.marker_orientation = quatern
        
        ps = PoseStamped()
        ps.pose.position = Point(.0,.0,.0)
        ps.pose.orientation = Quaternion(.0,.0,.0,.0)
        self.marker_ps = ps
        
        # 10hz
        loop_rate = rospy.Rate(10)
        while not rospy.is_shutdown():
            # publish the marker info onto different topics
            try:
                self.pub_marker_position.publish(self.marker_position)
                self.pub_marker_orientation.publish(self.marker_orientation)
                self.pub_marker_pose_stamped.publish(self.marker_ps)
                loop_rate.sleep()
            except rospy.ROSInterruptException:
                pass
        
    def cbGetMarkerOdom(self, markers_msg):
        for marker in markers_msg.markers:
            if marker.id == MARKER_ID_DETECTION:
                self.marker_orientation = marker.pose.pose.orientation
                self.marker_position = marker.pose.pose.position
                self.marker_ps = marker.pose

    def main(self):
        # spin() simply keeps python from exiting until this node is stopped
        rospy.spin()

if __name__ == '__main__':
    rospy.init_node('marker_info_py')
    node = MarkerInfo()
    node.main()
