#!/usr/bin/env python

# Author: Vincent Jadraque
# Student ID: 11970720
# Date: 29/10/2018
# File name: tf_bc.py

'''
This broadcasts the transforms between the robot hand and the camera
'''

import roslib
import rospy
import math
import tf
from geometry_msgs.msg import (
    PoseStamped,
    # Pose,
    #Point,
    #Quaternion,
)


def rpy2q(roll, pitch, yaw):
    return tf.transformations.quaternion_from_euler(roll, pitch, yaw)

if __name__ == '__main__':
    rospy.init_node('apc_tf_broadcaster')

    tl = tf.TransformListener()
    tb = tf.TransformBroadcaster()
    rate = rospy.Rate(20)
    # real world publisher
    pub_rw = rospy.Publisher('rw_base_hand',PoseStamped, queue_size = 1)

    hand2camera_length = 0.3 # Meters
    
    while not rospy.is_shutdown():
        try:
            time = tl.getLatestCommonTime('/right_l4', '/right_l5')

            # realsense camera wrt hand
            # TODO check actual distances and orientation for realsense camera
            #tb.sendTransform((0, 0, hand2camera_length), rpy2q(0, 0, 0), time, "camera_rgb_optical_frame", "right_hand")
            # laptop camera wrt hand
            rospy.loginfo("printing tf from hand to camera")
            tb.sendTransform((0, 0, hand2camera_length), rpy2q(0, 0, 0), time, "usb_cam","right_hand")
            (trans,rot) = tl.lookupTransform('base', 'right_hand', rospy.Time(0))
            print trans, rot
            rw_base_hand = PoseStamped()
            rw_base_hand.pose.position = trans
            rw_base_hand.pose.orientation = rot
            rw_base_hand.header.frame_id = 'end_effector'
            
            pub_rw.publish(rw_base_hand)

        except tf.Exception:
            rospy.loginfo("Exception Error")
            pass
        rate.sleep()