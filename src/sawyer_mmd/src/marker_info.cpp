/*
 * Copyright (c) 2010, Vincent Jadraque
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Willow Garage, Inc. nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

// %Tag(FULLTEXT)%
// %Tag(INCLUDES)%
#include <ros/ros.h>
//#include <ros/console.h>
#include <visualization_msgs/Marker.h>
#include <ar_track_alvar_msgs/AlvarMarkers.h>
//#include <geometry_msgs/PoseStamped.h>
//#include <geometry_msgs/Point.h>
//#include <geometry_msgs/Quaternion.h>

// %EndTag(INCLUDES)%

// %Tag(INIT)%

//float m_px, m_py, m_pz, m_ox, m_oy, m_oz, m_ow;
geometry_msgs::PoseStamped pose_stamped;
geometry_msgs::Pose marker_pose;
geometry_msgs::Point position;
geometry_msgs::Quaternion orientation;

void cbGetMarkerPose(const ar_track_alvar_msgs::AlvarMarkers::ConstPtr& msg)
{
  // retrieve the pose and posestamped messages from the first marker in the message
  pose_stamped = msg->markers.at(0).pose;
  marker_pose = pose_stamped.pose;
  position = marker_pose.position;
  orientation = marker_pose.orientation;
  ROS_INFO("working!");
}

int main( int argc, char** argv )
{
  ros::init(argc, argv, "marker_info");
  ros::NodeHandle n;
  ros::Rate r(10);
  // subscribe to ar_pose_marker topic
  ros::Subscriber sub = n.subscribe("ar_pose_marker", 1, cbGetMarkerPose);
  // publishers
  ros::Publisher pose_pub = n.advertise<geometry_msgs::PoseStamped>("pose_stamped", 1);
  ros::Publisher position_pub = n.advertise<geometry_msgs::Point>("marker_position", 1);
  ros::Publisher orientation_pub = n.advertise<geometry_msgs::Quaternion>("marker_orientation", 1);
  
// %EndTag(INIT)%

  while (ros::ok())
  {
    /*
    geometry_msgs::Point posn;
    geometry_msgs::Quaternion orntn;
    geometry_msgs::PoseStamped ps;
    
    posn = position;
    orntn = orientation;
    ps = pose_stamped;
    
    //roscpp.loginfo(
    
    // publish ar marker info
    
    pose_pub.publish(ps);
    position_pub.publish(posn);
    orientation_pub.publish(orntn);
    */
    try{
      ros::spinOnce();
    }
    catch (std::out_of_range oor) {
      ROS_ERROR("%s", oor.what());
    }
    
    pose_pub.publish(pose_stamped);
    position_pub.publish(position);
    orientation_pub.publish(orientation);
// %EndTag(PUBLISH)%

// %Tag(SLEEP_END)%
    r.sleep();
  }
// %EndTag(SLEEP_END)%
}
// %EndTag(FULLTEXT)%
