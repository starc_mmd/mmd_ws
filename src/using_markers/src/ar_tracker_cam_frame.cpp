/*
 * Copyright (c) 2010, Willow Garage, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Willow Garage, Inc. nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

// %Tag(FULLTEXT)%
// %Tag(INCLUDES)%
#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <ar_track_alvar_msgs/AlvarMarkers.h>
//#include <geometry_msgs/PoseStamped.h>
//#include <ros/console.h>

// %EndTag(INCLUDES)%

// %Tag(INIT)%

// double m_px, m_py, m_pz, m_ox, m_oy, m_oz, m_ow;
geometry_msgs::PoseStamped pose_stamped;
geometry_msgs::Pose marker_pose;
/*geometry_msgs::Point position;
geometry_msgs::Quaternion orientation;*/

void cbGetMarkerPose(const ar_track_alvar_msgs::AlvarMarkers::ConstPtr& msg)
{
  // retrieve the pose and posestamped messages from the first marker in the message
  /*
  for (i = 1; i < msg->markers.size(); i++) {
  }
  */
  pose_stamped = msg->markers.at(0).pose;
  marker_pose = pose_stamped.pose;
  /*position = marker_pose.position;
  orientation = marker_pose.orientation;*/
  m_pz = marker_pose.position.z;
}

int main( int argc, char** argv )
{
  ros::init(argc, argv, "ar_tracker");
  ros::NodeHandle n;
  // Hz
  ros::Rate r(50);
  // subscribe to ar_pose_marker topic
  ros::Subscriber sub = n.subscribe("ar_pose_marker", 1, cbGetMarkerPose);
  // publishers
  ros::Publisher marker_pub = n.advertise<visualization_msgs::Marker>("visualization_marker", 1);
  ros::Publisher pose_pub = n.advertise<geometry_msgs::Pose>("marker_pose", 1);
  //ros::Publisher pose_stamped_pub = n.advertise<geometry_msgs::PoseStamped>("pose_stamped", 1);
  //ros::Publisher position_pub = n.advertise<geometry_msgs::Point>("marker_position", 1);
  //ros::Publisher orientation_pub = n.advertise<geometry_msgs::Quaternion>("marker_orientation", 1);
  
// %EndTag(INIT)%

  // Set our initial shape type to be a cube
// %Tag(SHAPE_INIT)%
  uint32_t shape = visualization_msgs::Marker::ARROW;
// %EndTag(SHAPE_INIT)%

// %Tag(MARKER_INIT)%
  while (ros::ok())
  {
    try{
      ros::spinOnce();
    }
    catch (std::out_of_range oor) {
      ROS_ERROR("%s", oor.what());
    }
    
    visualization_msgs::Marker marker;
    /*
    geometry_msgs::Point posn;
    geometry_msgs::Quaternion orntn;
    geometry_msgs::PoseStamped ps;*/
    // Set the frame ID and timestamp.  See the TF tutorials for information on these.
    marker.header.frame_id = "/usb_cam";
    marker.header.stamp = ros::Time::now();
// %EndTag(MARKER_INIT)%

    // Set the namespace and id for this marker.  This serves to create a unique ID
    // Any marker sent with the same namespace and id will overwrite the old one
// %Tag(NS_ID)%
    marker.ns = "ar_tracker";
    marker.id = 0;
// %EndTag(NS_ID)%

    // Set the marker type.  Initially this is CUBE, and cycles between that and SPHERE, ARROW, and CYLINDER
// %Tag(TYPE)%
    marker.type = shape;
// %EndTag(TYPE)%

    // Set the marker action.  Options are ADD, DELETE, and new in ROS Indigo: 3 (DELETEALL)
// %Tag(ACTION)%
    marker.action = visualization_msgs::Marker::ADD;
// %EndTag(ACTION)%

    // Set the pose of the marker.  This is a full 6DOF pose relative to the frame/time specified in the header
// %Tag(POSE)%
    marker.pose = marker_pose;
// %EndTag(POSE)%

    // Set the scale of the marker -- 1x1x1 here means 1m on a side
// %Tag(SCALE)%
    marker.scale.x = 1.0;
    marker.scale.y = 1.0;
    marker.scale.z = 1.0;
// %EndTag(SCALE)%

    // Set the color -- be sure to set alpha to something non-zero!
// %Tag(COLOR)%
    marker.color.r = 0.0f;
    marker.color.g = 1.0f;
    marker.color.b = 0.0f;
    marker.color.a = 1.0;
// %EndTag(COLOR)%

// %Tag(LIFETIME)%
    marker.lifetime = ros::Duration();
// %EndTag(LIFETIME)%

    // Publish the marker
// %Tag(PUBLISH)%
    while (marker_pub.getNumSubscribers() < 1)
    {
      if (!ros::ok())
      {
        return 0;
      }
      ROS_WARN_ONCE("Please create a subscriber to the marker");
      sleep(1);
    }
    ROS_INFO("x = %f", marker.pose.position.x);
    ROS_INFO("y = %f", marker_pose.position.y);
    ROS_INFO("z = %f", m_pz);
    marker_pub.publish(marker);
    
    //pose_pub.publish(marker_pose);
    
    /*
    posn = position;
    orntn = orientation;
    ps = pose_stamped;
    
    // publish ar marker info
    pose_stamped_pub.publish(ps);
    position_pub.publish(posn);
    orientation_pub.publish(orntn);*/
// %EndTag(PUBLISH)%

    // Cycle between different shapes
// %Tag(CYCLE_SHAPES)%
    /*switch (shape)
    {
    case visualization_msgs::Marker::CUBE:
      shape = visualization_msgs::Marker::SPHERE;
      break;
    case visualization_msgs::Marker::SPHERE:
      shape = visualization_msgs::Marker::ARROW;
      break;
    case visualization_msgs::Marker::ARROW:
      shape = visualization_msgs::Marker::CYLINDER;
      break;
    case visualization_msgs::Marker::CYLINDER:
      shape = visualization_msgs::Marker::CUBE;
      break;
    }*/
// %EndTag(CYCLE_SHAPES)%

// %Tag(SLEEP_END)%
    r.sleep();
  }
// %EndTag(SLEEP_END)%
}
// %EndTag(FULLTEXT)%
